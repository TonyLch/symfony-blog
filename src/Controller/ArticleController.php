<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleFormType;
use App\Form\CommentFormType;
use App\Repository\CommentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    /**
     * @Route("/article/{id}/{title}", name="article")
     */
    public function article(int $id, string $title, CommentRepository $commentRepository, Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $article = $repo->find($id);

        $comments = $commentRepository->findAllValidById($id);

        $comment = new Comment();

        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $content = $form["content"]->getData();
            $comment
                ->setValidated(false)
                ->setAuthor($this->getUser())
                ->setArticle($article)
                ->setDate(new \DateTime())
                ->setContent($content);

            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash('comment', "Commentaire créé !");
            $this->addFlash('comment', "Votre commentaire est en attente de validation...");

            return $this->redirect($this->generateUrl('article', ['id' => $id, 'title' => $title]));
        }

        return $this->render('pages/article.html.twig', [
            'article' => $article,
            'comments' => $comments,
            'commentForm' => $form->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/article/create", name="create_article")
     */
    public function createArticle(Request $request): Response
    {
        $article = new Article();

        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $visibility = $form["visibility"]->getData();
            $title = $form["title"]->getData();
            $subheading = $form["subheading"]->getData();
            $image = $form["image"]->getData();
            $content = $form["content"]->getData();
            $category = $form["category"]->getData();

            $article
                ->setVisibility($visibility)
                ->setAuthor($this->getUser())
                ->setCategory($category)
                ->setDate(new \DateTime())
                ->setTitle($title)
                ->setSubheading($subheading)
                ->setImage($image)
                ->setContent($content);

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('article', "Article créé !");

            return $this->redirect($this->generateUrl('create_article'));
        }

        return $this->render('pages/create_article.html.twig', [
            'articleForm' => $form->createView()
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/update/{id}", name="update_article")
     */
    public function updateArticle(int $id, Request $request): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $article = $repo->find($id);

        $form = $this->createForm(ArticleFormType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $visibility = $form["visibility"]->getData();
            $title = $form["title"]->getData();
            $subheading = $form["subheading"]->getData();
            $image = $form["image"]->getData();
            $content = $form["content"]->getData();
            $category = $form["category"]->getData();

            $article
                ->setVisibility($visibility)
                ->setAuthor($this->getUser())
                ->setCategory($category)
                ->setDate(new \DateTime())
                ->setTitle($title)
                ->setSubheading($subheading)
                ->setImage($image)
                ->setContent($content);

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('update_article', "Article modifié !");

            return $this->redirect($this->generateUrl('update_article', [
                'id' => $id
            ]));
        }

        return $this->render('pages/update_article.html.twig', [
            'updateForm' => $form->createView(),
            'id' => $id
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/delete/{id}", name="delete_article")
     */
    public function deleteArticle(int $id): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        $this->addFlash('delete_article', "Article supprimé.");

        return $this->redirect($this->generateUrl('dashboard_admin', [
            'tab' => 'articles'
        ]));
    }
}
