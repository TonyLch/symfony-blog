<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Share;
use App\Form\RegistrationFormType;
use App\Repository\CommentRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DashboardController extends AbstractController
{

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/dashboard/admin/{tab}", name="dashboard_admin")
     */
    public function dashboardAdmin(string $tab, CommentRepository $commentRepository): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $articles = $repo->findBy(
            ['author' => $this->getUser()]
        );
        $tabComponent = '';

        $comments = [];

        foreach ($articles as $value) {
            $comments = array_merge($comments, $commentRepository->findAllValidById($value->getId(), false));
        }

        switch ($tab) {
            case 'home':
                $tabComponent = 'components/dashboard/admin/home.html.twig';
                break;
            case 'articles':
                $tabComponent = 'components/dashboard/admin/articles.html.twig';
                break;
            case 'comments':
                $tabComponent = 'components/dashboard/admin/comments.html.twig';
                break;

            default:
                $tabComponent = 'components/dashboard/admin/home.html.twig';
                break;
        }

        return $this->render('pages/dashboard.html.twig', [
            'articles' => $articles,
            'comments' => $comments,
            'tab' => $tabComponent
        ]);
    }

    /**
     * @Route("/dashboard/{tab}", name="dashboard")
     */
    public function dashboard(string $tab, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $repoArticle = $this->getDoctrine()->getRepository(Article::class);
        $tabComponent = '';

        // LIKE
        $repoLike = $this->getDoctrine()->getRepository(Like::class);

        $likes = $repoLike->findBy(
            ['user' => $this->getUser()]
        );

        $likeArticles = [];

        foreach ($likes as $value) {
            array_push($likeArticles, $repoArticle->find($value->getArticle()->getId()));
        }

        // SHARE
        $repoShare = $this->getDoctrine()->getRepository(Share::class);

        $shares = $repoShare->findBy(
            ['user' => $this->getUser()]
        );

        $shareArticles = [];

        foreach ($shares as $value) {
            array_push($shareArticles, $repoArticle->find($value->getArticle()->getId()));
        }

        // COMMENT
        $repoComment = $this->getDoctrine()->getRepository(Comment::class);

        $comments = $repoComment->findBy(
            ['author' => $this->getUser()]
        );

        $commentArticles = [];

        foreach ($comments as $value) {
            array_push($commentArticles, $repoArticle->find($value->getArticle()->getId()));
        }


        // DETAIL FORM

        if ($this->getUser())
            $roles = ['roles' => $this->getUser()->getRoles()];
        else
            $roles = ['roles' => []];

        $user = $this->getUser();

        $form = $this->createForm(RegistrationFormType::class, $user, $roles);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard', [
                'tab' => $tabComponent
            ]);
        }

        switch ($tab) {
            case 'home':
                $tabComponent = 'components/dashboard/user/home.html.twig';
                break;
            case 'details':
                $tabComponent = 'components/dashboard/user/details.html.twig';
                break;
            case 'liked':
                $tabComponent = 'components/dashboard/user/liked.html.twig';
                break;
            case 'shared':
                $tabComponent = 'components/dashboard/user/shared.html.twig';
                break;
            case 'commented':
                $tabComponent = 'components/dashboard/user/commented.html.twig';
                break;

            default:
                $tabComponent = 'components/dashboard/user/home.html.twig';
                break;
        }

        return $this->render('pages/dashboard.html.twig', [
            'likeArticles' => $likeArticles,
            'shareArticles' => $shareArticles,
            'commentArticles' => $commentArticles,
            'form' => $form->createView(),
            'tab' => $tabComponent
        ]);
    }
}
