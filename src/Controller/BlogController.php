<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Like;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function home(ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->findAllOrderByAuthor('ASC');

        return $this->render('pages/home.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about(): Response
    {
        return $this->render('pages/about.html.twig', []);
    }

    /**
     * @Route("/article/{id}/{title}/like", name="like")
     */
    public function like(int $id, string $title): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $article = $repo->find($id);

        $like = new Like();

        $entityManager = $this->getDoctrine()->getManager();

        $like
            ->setUser($this->getUser())
            ->setArticle($article);

        $entityManager->persist($like);
        $entityManager->flush();

        $this->addFlash('like', "Vous aimez cet article!");

        return $this->redirect($this->generateUrl('article', ['id' => $id, 'title' => $title]));
    }

    /**
     * @Route("/article/{id}/{title}/dislike", name="dislike")
     */
    public function dislike(int $id, string $title): Response
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $article = $repo->find($id);
        $articleLikes = $article->getLikes();
        $userLikes = $this->getUser()->getLikes();

        $entityManager = $this->getDoctrine()->getManager();

        foreach ($articleLikes as $key => $value) {
            if ($userLikes[$key] === $value)
                $entityManager->remove($value);
        }

        $entityManager->flush();

        $this->addFlash('dislike', "Vous n'aimez plus cet article...");

        return $this->redirect($this->generateUrl('article', ['id' => $id, 'title' => $title]));
    }
}
