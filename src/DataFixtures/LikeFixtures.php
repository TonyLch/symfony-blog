<?php

namespace App\DataFixtures;

use App\Entity\Like;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{
    public const LIKE_REFERENCE = 'like';

    public function load(ObjectManager $manager)
    {
        // User
        for ($i = 10; $i < 26; $i++) {
            $like = new Like();
            $like
                ->setUser($this->getReference('user_user'))
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)));
            $manager->persist($like);
            $this->addReference(self::LIKE_REFERENCE . '_' . $i, $like);
        }

        for ($i = 0; $i < 10; $i++) {
            $like = new Like();
            $like
                ->setUser($this->getReference(UserFixtures::USER_REFERENCE . '_' . rand(0, 9)))
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)));
            $manager->persist($like);
            $this->addReference(self::LIKE_REFERENCE . '_' . $i, $like);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ArticleFixtures::class
        ];
    }
}
