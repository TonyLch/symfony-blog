<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public const COMMENT_REFERENCE = 'comment';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 10; $i < 26; $i++) {
            $comment = new Comment();
            $comment
                ->setValidated(true)
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)))
                ->setDate($faker->dateTime($max = 'now', $timezone = null))
                ->setContent($faker->realText($maxNbChars = 200, $indexSize = 1));

            if ($i % 2 == 0)
                $comment->setAuthor($this->getReference('user_admin'));
            else $comment->setAuthor($this->getReference('user_user'));

            $manager->persist($comment);
            $this->addReference(self::COMMENT_REFERENCE . '_' . $i, $comment);
        }

        for ($i = 0; $i < 10; $i++) {
            $comment = new Comment();
            $comment
                ->setValidated(true)
                ->setAuthor($this->getReference(UserFixtures::USER_REFERENCE . '_' . rand(0, 9)))
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)))
                ->setDate($faker->dateTime($max = 'now', $timezone = null))
                ->setContent($faker->realText($maxNbChars = 200, $indexSize = 1));
            $manager->persist($comment);
            $this->addReference(self::COMMENT_REFERENCE . '_' . $i, $comment);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ArticleFixtures::class
        ];
    }
}
