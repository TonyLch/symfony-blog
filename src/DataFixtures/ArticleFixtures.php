<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public const ARTICLE_REFERENCE = 'article';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        // Articles for Admin & User account
        for ($i = 10; $i < 26; $i++) {
            $article = new Article();
            $article
                ->setVisibility(true)
                ->setDate($faker->dateTime($max = 'now', $timezone = null))
                ->setTitle($faker->jobTitle)
                ->setSubheading($faker->catchPhrase)
                ->setImage($faker->imageUrl($width = 640, $height = 480))
                ->setContent($faker->realText($maxNbChars = 1000, $indexSize = 1))
                ->setAuthor($this->getReference('user_admin'))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORY_REFERENCE . '_' . rand(0, 9)));

            $manager->persist($article);
            $this->addReference(self::ARTICLE_REFERENCE . '_' . $i, $article);
        }

        // Articles for random users
        for ($i = 0; $i < 10; $i++) {
            $article = new Article();
            $article
                ->setVisibility(true)
                ->setDate($faker->dateTime($max = 'now', $timezone = null))
                ->setTitle($faker->jobTitle)
                ->setSubheading($faker->catchPhrase)
                ->setImage($faker->imageUrl($width = 640, $height = 480))
                ->setContent($faker->realText($maxNbChars = 1000, $indexSize = 1))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORY_REFERENCE . '_' . rand(0, 9)));


            $evenRandomNb = rand(0, round((9 / 2), 0, PHP_ROUND_HALF_DOWN)) * 2;
            $article->setAuthor($this->getReference(UserFixtures::USER_REFERENCE . '_' . $evenRandomNb));

            $manager->persist($article);
            $this->addReference(self::ARTICLE_REFERENCE . '_' . $i, $article);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class
        ];
    }
}
