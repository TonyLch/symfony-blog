<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ContactFixtures extends Fixture
{
    public const CONTACT_REFERENCE = 'contact';

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $contact = new Contact();
            $contact
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPhone($faker->phoneNumber())
                ->setMessage($faker->realText($maxNbChars = 200, $indexSize = 1));

            $manager->persist($contact);
            $this->addReference(self::CONTACT_REFERENCE . '_' . $i, $contact);
        }

        $manager->flush();
    }
}
