<?php

namespace App\DataFixtures;

use App\Entity\Share;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ShareFixtures extends Fixture implements DependentFixtureInterface
{
    public const SHARE_REFERENCE = 'share';

    public function load(ObjectManager $manager)
    {
        // User
        for ($i = 10; $i < 26; $i++) {
            $share = new Share();
            $share
                ->setUser($this->getReference('user_user'))
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)));
            $manager->persist($share);
            $this->addReference(self::SHARE_REFERENCE . '_' . $i, $share);
        }

        for ($i = 0; $i < 10; $i++) {
            $share = new Share();
            $share
                ->setUser($this->getReference(UserFixtures::USER_REFERENCE . '_' . rand(0, 9)))
                ->setArticle($this->getReference(ArticleFixtures::ARTICLE_REFERENCE . '_' . rand(0, 9)));
            $manager->persist($share);
            $this->addReference(self::SHARE_REFERENCE . '_' . $i, $share);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            ArticleFixtures::class
        ];
    }
}
