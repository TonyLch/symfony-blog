<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const USER_REFERENCE = 'user';
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $admin = new User();
        $admin
            ->setEmail('admin@admin.com')
            ->setPassword($this->encoder->encodePassword($admin, 'admin'))
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPseudo($faker->firstName)
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);
        $this->addReference('user_admin', $admin);

        $user = new User();
        $user
            ->setEmail('user@user.com')
            ->setPassword($this->encoder->encodePassword($user, 'user'))
            ->setFirstName($faker->firstName)
            ->setLastName($faker->lastName)
            ->setPseudo($faker->firstName);

        $manager->persist($user);
        $this->addReference('user_user', $user);

        for ($i = 0; $i < 10; $i++) {
            $users = new User();
            $users
                ->setEmail($faker->email)
                ->setPassword($this->encoder->encodePassword($users, $faker->password()))
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setPseudo($faker->firstName);

            if ($i % 2 === 0)
                $users->setRoles(['ROLE_ADMIN']);

            $manager->persist($users);
            $this->addReference(self::USER_REFERENCE . '_' . $i, $users);
        }

        $manager->flush();
    }
}
