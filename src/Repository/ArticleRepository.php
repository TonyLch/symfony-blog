<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findAllOrderByAuthor(string $order = 'ASC', bool $visible = true)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->innerJoin('a.author', 'aa', 'with', 'a.author=aa.id')
            ->orderBy('aa.pseudo', $order);

        if ($visible) {
            $queryBuilder->andWhere('a.visibility=true');
        }

        $result = $queryBuilder->getQuery()->getResult();

        return $result;
    }

    public function findAllOrderByDate(string $order = 'ASC'/*, bool $visible = true*/)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->orderBy('a.date', $order);

        // if ($visible) {
        //     $queryBuilder->andWhere('a.visible = TRUE');
        // }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllOrderByTitle(string $order = 'ASC'/*, bool $visible = true*/)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->orderBy('a.title', $order);

        // if ($visible) {
        //     $queryBuilder->andWhere('a.visible = TRUE');
        // }

        return $queryBuilder->getQuery()->getResult();
    }
}
