# Symfony - Blog

School Project

I rushed a little bit the project and haven't finished everything yet. So this isn't the cleanest project since I learned a lot while doing it. As I say before, this was a messy project, features were all over the place, the start wasn't that bad but when I saw the time left, I rushed without thinking about the quality of my code.

This was a pretty fun project but I failed at orgnizing my time. And the number of solutions to reach a goal in Symfony sometimes tricks me.

The hardest point for me was actually to have a full control on my project. I had a hard time at debugging and with the management of my files.

## Fixtures

**Admin account** : 
- id : admin@admin.com
- mdp : admin

**User account** : 
- id : user@user.com
- mdp : user